﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		transform.localRotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
